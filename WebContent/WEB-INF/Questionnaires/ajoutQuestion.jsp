<html>
	<head>
		<title>Ajout d'une question</title>
	</head>
	<body>
		<h1>Ajout d'une question au questionnaire ${ questionnaire.sujet  }</h1>
		
		<p><font color="red">${ requestScope.err }</font></p>
		<p><font color="green">${ requestScope.succes }</font></p>
		
		<form method="post" action="ajoutQuestion">
			<input type="hidden" name="id-questionnaire" value="${ questionnaire.id }"/>
			<p>Sujet du questionnaire: <input type="text" name="input-sujet" value="${ questionnaire.sujet }" readOnly/></p>	
			<p>Intitul� question :</p>
			<p><textarea rows="3" cols="60" name="input-intitule" required autofocus></textarea></p>
			
			<p><input type="submit" name="ajoutReponse" value="Valider et ajouter des r�ponses possibles"/><input type="submit" name="terminer" value="Terminer"/></p>
		</form>
	</body>
</html>
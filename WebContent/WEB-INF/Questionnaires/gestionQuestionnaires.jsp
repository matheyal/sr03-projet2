<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<title>Gestion des questionnaires</title>
	</head>
	<body>
		<h1>Gestion des questionnaires</h1>
		
		<p><font color="red">${ requestScope.err }</font></p>
		<p><font color="green">${ requestScope.succes }</font></p>
		
		<form action="ajoutQuestionnaire">
			<p><input type="submit" value="Ajouter un questionnaire"/></p>
		</form>
		
		<!-- Affichage des questionnaires existants
		- chaque questionnaire sous la forme d'un lien qui redirige vers la page de modification/suppression des questionnaires
		- pagination des r�sultats
		-->
		<table border="1" >
	        <tr>
	            <th>Sujet</th>
				<th>Nombre de questions</th>	            
	            <th>Statut</th>
	            <th>D�tails</th>
	        </tr>
	        <c:forEach items="${ questList }" var="quest">
	            <tr>
	                <td>${quest.sujet}</td>
	                <td>${quest.nbQuestions }</td>
	                <td>${quest.statut}</td>
	                
	                <td><a href="modifQuestionnaire?idQuestionnaire=${ quest.id }">Modifier</a></td>
	                
	            </tr>
	        </c:forEach>
	    </table>
		
		
	</body>
	<%@ include file="/WEB-INF/footer.jsp" %>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<title>Modification de questionnaire</title>
	</head>
	<body>
		<h1>Modification du questionnaire ${ requestScope.sujetQuestionnaire }</h1>
		<!-- Affichage des questions du questionnaire
		- chaque question sous la forme d'un lien qui redirige vers la page de modification/suppression des questions
		- pagination des résultats
		-->
		
		<!---->
		
		<h2>Informations du questionnaire</h2>
		<form method="post" action="modifQuestionnaire">
			<p>ID* : <input type="text" name="input-id" value="${ questionnaire.id }" readOnly/></p>
			<p>Sujet* : <input type="text" name="input-sujet" value="${ questionnaire.sujet }" autofocus required/></p>
			<p>Actif : <input type="checkbox" name="input-statut" ${ questionnaire.statut == true ? 'checked' : '' }/></p>
			<p><input type="submit" value="Valider les modifications" name="modifier"/><input type="submit" value="Supprimer" name="supprimer"/></p>
			<p>* : Champs obligatoires</p>
		</form>
		
		<h2>Liste des questions</h2>
		<table border="1" >
	        <tr>
	            <th>Intitulé</th>
				<th>Actif</th>
	        </tr>
	        <c:forEach items="${ questionsList }" var="quest">
	            <tr>
	                <td>${quest.intitule}</td>
	                <td>${quest.statut}</td>
	                
	                <td><a href="modifQuestion?id=${ quest.idQuestion }">Modifier</a></td>
	                
	            </tr>
	        </c:forEach>
	    </table>
		
		<form method="post" action="ajoutQuestion">
			<input type="hidden" value="${ questionnaire.id }" name="id-questionnaire"/>
			<p><input type="submit" name="ajoutReponse" value="Ajouter une question"/></p>
		</form>
		
			
	</body>
</html>
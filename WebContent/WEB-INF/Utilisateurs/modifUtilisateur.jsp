<html>
	<head>
		<title>Modification d'un utilisateur</title>
	</head>
	<body>
		<h1><a href="index">Modification d'un utilisateur</a></h1>
		
		<p><font color="red">${ requestScope.err }</font></p>
		<p><font color="green">${ requestScope.succes }</font></p>
		
		<p>Modifiez les informations d�sir�es :</p>
		<form method="post" action="modifUtilisateur">
			<p>Login* : <input type="text" name="input-login" value="${ user.login }" readOnly required/></p>
			<p>Nom et pr�nom* : <input type="text" name="input-nom" value="${ user.nom }" autofocus required/></p>
			<p>Email* : <input type="text" name="input-email" value="${ user.email }" required/></p>
			<p>Mot de passe* : <input type="text" name="input-mdp" value="${ user.mdp }" required/></p>
			<p>Soci�t� : <input type="text" name="input-societe"  value="${ user.societe }"/></p>
			<p>T�l�phone : <input type="text" name="input-telephone"  value="${ user.telephone }"/></p>
			<p>Administrateur : <input type="checkbox" name="input-admin" ${ user.admin == true ? 'checked' : '' }/></p>
			<p><input type="submit" value="Modifier" name="modifier"/><input type="submit" value="Supprimer" name="supprimer"/></p>
			<p>* : Champs obligatoires</p>
		</form>
	</body>
	<%@ include file="/WEB-INF/footer.jsp" %>
</html>
<html>
	<head>
		<title>Ajout d'un utilisateur</title>
	</head>
	<body>
		<h1><a href="index">Ajout d'un utilisateur</a></h1>
		<p><font color="red">${ requestScope.err }</font></p>
		<p><font color="green">${ requestScope.succes }</font></p>
		<p>Veuillez remplir les informations ci-dessous :</p>
		<form method="post" action="ajoutUtilisateur">
			<p>Login* : <input type="text" name="input-login" value="${requestScope.inputLogin}" autofocus required/></p>
			<p>Nom et pr�nom* : <input type="text" name="input-nom" value="${requestScope.inputNom}" required/></p>
			<p>Email* : <input type="text" name="input-email" value="${requestScope.inputEmail}" required/></p>
			<p>Mot de passe* : <input type="text" name="input-mdp" value="${requestScope.inputMdp}" required/></p>
			<p>Soci�t� : <input type="text" name="input-societe" value="${ requestScope.inputSociete }" /></p>
			<p>T�l�phone : <input type="text" name="input-telephone" value="${requestScope.inputTelephone}" /></p>
			<p>Administrateur : <input type="checkbox" name="input-admin" ${ requestScope.inputAdmin == true ? 'checked' : '' }/></p>
			<p><input type="submit" value="Ajouter un utilisateur"/></p>
			<p>* : Champs obligatoires</p>
		</form>
	</body>
	<%@ include file="/WEB-INF/footer.jsp" %>
</html>
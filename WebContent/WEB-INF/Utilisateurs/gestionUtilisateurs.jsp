<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<title>Gestion des utilisateurs</title>
		<link rel="stylesheet" type="text/css" href="style.css" />
	</head>
	<body>
		<h1><a href="index">Gestion des utilisateurs</a></h1>
		
		<p><font color="red">${ requestScope.err }</font></p>
		<p><font color="green">${ requestScope.succes }</font></p>
		
		<form action="ajoutUtilisateur">
			<p><input type="submit" value="Ajouter un utilisateur"/></p>
		</form>
		<c:set var="nbUsers" scope="session">
		   <c:out value="${nbUsers}"/>
		</c:set>
		
		<table border="1" >
	        <tr>
	            <th>Login</th>
	            <th>Nom</th>
	            <th>Email</th>
	            <th>Societe</th>
	            <th>T�l�phone</th>
	            <th>Date de cr�ation</th>
	            <th>Admin</th>
	        </tr>
	 
	       	<c:forEach items="${ usersList }" var="user">
	            <tr>
	                <td>${user.login}</td>
	                <td>${user.nom}</td>
	                <td>${user.email}</td>
	                <td>${user.societe}</td>
	                <td>${user.telephone}</td>
	                <td>${user.dateCreation}</td>
	                <td>${user.admin}</td>
	                <td><a href="modifUtilisateur?login=${ user.login }">Modifier</a></td>
	                <td><a href="visuParcours?login=${ user.login }">Voir les parcours</a></td>
	            </tr>
	        </c:forEach>
	    </table>
	    
	    <table border="0">
	    	<tr>
			    <%--For displaying Previous link except for the 1st page --%>
			    <c:if test="${currentPage != 1}">
			        <td><a href="gestionUtilisateurs?page=${currentPage - 1}">Pr�c�dent</a></td>
			    </c:if>
			 
			    <%--For displaying Page numbers. 
			    The when condition does not display a link for the current page--%>
		            <c:forEach begin="1" end="${nbPages}" var="i">
		                <c:choose>
		                    <c:when test="${currentPage eq i}">
		                        <td>${i}</td>
		                    </c:when>
		                    <c:otherwise>
		                        <td><a href="gestionUtilisateurs?page=${i}">${i}</a></td>
		                    </c:otherwise>
		                </c:choose>
		            </c:forEach>
			     
			    <%--For displaying Next link --%>
			    <c:if test="${currentPage lt nbPages}">
			        <td><a href="gestionUtilisateurs?page=${currentPage + 1}">Suivant</a></td>
			    </c:if>
		    </tr>
	    </table>
	</body>
	<%@ include file="/WEB-INF/footer.jsp" %>
</html>
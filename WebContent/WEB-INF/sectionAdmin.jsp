<html>
	<head>
		<title>Page administration</title>
	</head>
	<body>
		<h1>Administration de la plateforme de contr�le des connaissances</h1>
		<p><font color="red">${ requestScope.err }</font></p>
		<p>Bonjour ${ sessionScope.login } !</p>
		
		<ul>
			<li><a href="gestionUtilisateurs">G�rer les utilisateurs</a></li>
			<li><a href="gestionQuestionnaires">G�rer les questionnaires</a></li>
		</ul>
	</body>
	<%@ include file="/WEB-INF/footer.jsp" %>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<title>Page stagiaire</title>
		<link rel="stylesheet" type="text/css" href="style.css" />
	</head>
	<body>
		<h1>Consultation questionnaires</h1>
		
		<p><font color="red">${ requestScope.err }</font></p>
		<p><font color="green">${ requestScope.succes }</font></p>
		
		<p>Bonjour ${ sessionScope.login } !</p>
		
		<h3>Questionnaires disponibles</h3>
		<!-- Liste des questionnaire pas encore r�alis�s -->
		<table border="1" >
	        <tr>
	            <th>Sujet</th>
	            <th>Nombre de questions</th>
	            <th></th>
	        </tr>
	 
	       	<c:forEach items="${ listeTests }" var="test">
	            <tr>
	                <td>${test.sujet}</td>
	                <td>${test.nbQuestions}</td>
	                <td><a href="">R�pondre au test</a></td>
	            </tr>
	        </c:forEach>
	    </table>
		
		<h3>Questionnaires termin�s</h3>
		<!-- Liste des questionnaires termin�s avec dur�es et scores -->
		<table border="1" >
	        <tr>
	            <th>Sujet</th>
	            <th>Score</th>
	            <th>Dur�e</th>
	        </tr>
	 
	       	<c:forEach items="${ listeParcours }" var="parcours">
	            <tr>
	                <td>${parcours.sujet}</td>
	                <td>${parcours.score}</td>
	                <td>${parcours.dur�e}</td>
	            </tr>
	        </c:forEach>
	    </table>
		
	</body>
	<%@ include file="/WEB-INF/footer.jsp" %>
</html>
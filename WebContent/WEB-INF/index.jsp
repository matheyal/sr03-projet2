<html>
	<head>
		<title>Authentification</title>
	</head>
	<body>
		<h1>Plateforme d'évaluation stagiaire</h1>
		<p><font color="red">${ requestScope.err }</font></p>
		<form id="form-auth" method="post" action="authentification">
			<p><input id="input-login" name="input-login" type="text" placeholder="login" autofocus required/></p>
			<p><input id="input-pwd" name="input-pwd" type="password" placeholder="password" required/></p>
			<p><input id="submit-auth" type="submit" value="Log In"/></p>
		</form>
	</body>
</html>
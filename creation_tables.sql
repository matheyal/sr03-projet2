CREATE TABLE utilisateur (
	login VARCHAR(30) NOT NULL,
	email VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	admin BOOLEAN NOT NULL,
	dateCreation TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	nom VARCHAR(255) NOT NULL,
	societe VARCHAR(255),
	telephone VARCHAR(25),
	isActive BOOLEAN,
	PRIMARY KEY (login),
	CHECK (LEN(password) >= 6)
);

CREATE TABLE test (
	id_test INTEGER NOT NULL AUTO_INCREMENT,
	sujet VARCHAR(700) NOT NULL UNIQUE,
	isActive BOOLEAN NOT NULL,
	PRIMARY KEY (id_test)
);

CREATE TABLE parcours (
	id_parcours INTEGER NOT NULL AUTO_INCREMENT,
	fk_utilisateur VARCHAR(30) NOT NULL,
	fk_test INTEGER NOT NULL,
	score INTEGER NOT NULL,
	duree TIME NOT NULL,
	PRIMARY KEY (id_parcours),
	FOREIGN KEY (fk_test) REFERENCES test(id_test),
	FOREIGN KEY (fk_utilisateur) REFERENCES utilisateur(login) ON DELETE CASCADE
);

CREATE TABLE question (
	id_question INTEGER NOT NULL AUTO_INCREMENT,
	intitule VARCHAR(700) NOT NULL,
	fk_test INTEGER NOT NULL,
	isActive BOOLEAN,
	PRIMARY KEY (id_question),
	FOREIGN KEY (fk_test) REFERENCES test(id_test) ON DELETE CASCADE
);

CREATE TABLE reponse (
	id_reponse INTEGER NOT NULL AUTO_INCREMENT,
	fk_question INTEGER NOT NULL,
	intitule VARCHAR(700) NOT NULL,
	isActive BOOLEAN NOT NULL,
	isTrue BOOLEAN NOT NULL,
	PRIMARY KEY (id_reponse),
	FOREIGN KEY (fk_question) REFERENCES question(id_question) ON DELETE CASCADE
);

CREATE TABLE reponse_parcours (
	fk_parcours INTEGER NOT NULL,
	fk_reponse INTEGER NOT NULL,
	PRIMARY KEY (fk_parcours, fk_reponse),
	FOREIGN KEY (fk_parcours) REFERENCES parcours(id_parcours) ON DELETE CASCADE,
	FOREIGN KEY (fk_reponse) REFERENCES reponse(id_reponse) ON DELETE CASCADE
);

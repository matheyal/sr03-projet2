# Application web d'évaluation des compétences des stagiaires

## Fonctionnalités
* Gestion des utilisateurs
* Gestion des questionnaires
* Gestion des résultats
* Évaluation des compétences

## Description fonctionnelle
* **Utilisateur**
  * Administrateur ou stagiaire
  * Authentification
    * adresse e-mail
    * mot de passe (6 caractères minimum)
  * Nom
  * Société
  * Coordonnées téléphoniques
  * Date de création du compte
  * statut du compte (actif/inactif)

> Seul un administrateur peut créer un compte de stagiaire. Lors de la création, un mail automatique est envoyé au stagiaire avec son e-mail et mdp.

* **Questionnaire**
  * Associé à un sujet unique
  * Plusieurs questions :
    * liste de réponses possibles (une réponse possible associée à une seule question)
    * une seule bonne réponse
  * Chaque entité (questionnaire/question/réponse) possède un statut (actif/inactif)

> L'admin soit pouvoir changer l'ordre des questions d'un questionnaire ainsi que l'ordre des réponses possibles à une question

* **Parcours** : sauvegarde du passage d'un questionnaire
  * Utilisateur  ayant fait le questionnaire
  * Questionnaire fait
  * Score (bonne réponse = 1 pt, mauvaise réponse = 0 pt)
  * Durée du passage
  * Liste des réponses formulées

> À la fin d'un questionnaire, on affiche au stagiaire son score, la durée et les bonnes réponses

## Enchaînement des écrans
### Page de login
Tout utilisateur arrive sur une page de login demandant son login et son mdp. Selon le type de compte, l'utilisateur est redirigé vers une page différente.

### La vue Administrateur
* **Gestion utilisateurs :**
  * visualisation des comptes (pagination, recherche)
  * création de comptes
  * Ajout/modification/suppression
  * Détail des utilisateurs :
    * liste des parcours effectués (score, durée)
    * Pour chaque parcours ajout du meilleur score obtenu et classement de l'utilisateur face aux autres stagiaires
* **Gestion des questionnaiires :**
  * visualisation des questionnaires (pagination, recherche)
  * Ajout/Suppression
  * Modification : **Gestion des questions**
    * Affichage de la liste des questions du questionnaire (pagination, recherche)
    * Ajout/Suppression
    * Modification : **Gestion des réponses**
      * Affiche la liste des réponses possibles à la question (pagination, recherche)
      * Ajout/Suppression
      * Modification

### La vue stagiaire
* Visualisation de la liste des questionnaires (pagination, recherche).
  * Choix d'un questionnaire entraîne le début d'un parcours
* Visualisation des résultats obtenus

## Tehcnologies
* Java/J2EE
* Serveur d'applciation : Tomcat ou JBoss
* SGBD : MySQL ou autre

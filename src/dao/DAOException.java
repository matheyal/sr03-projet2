package dao;

import java.sql.SQLException;
import java.util.Hashtable;

public class DAOException extends Exception {
	private static final long serialVersionUID = 1L;
	private static Hashtable<String, String> sqlStateMessages;
	
	static {
        sqlStateMessages = new Hashtable<String, String>();
        sqlStateMessages.put("01002", "Disconnect error");
        sqlStateMessages.put("01004", "Data truncated");
        sqlStateMessages.put("01006", "Privilege not revoked");
        sqlStateMessages.put("01S00", "Invalid connection string attribute");
        sqlStateMessages.put("01S01", "Error in row");
        sqlStateMessages.put("01S03", "No rows updated or deleted");
        sqlStateMessages.put("01S04", "More than one row updated or deleted");
        sqlStateMessages.put("07001", "Wrong number of parameters");
        sqlStateMessages.put("08001", "Unable to connect to data source");
        sqlStateMessages.put("08002", "Connection in use");
        sqlStateMessages.put("08003", "Connection not open");
        sqlStateMessages.put("08004", "Data source rejected establishment of connection");
        sqlStateMessages.put("08007", "Connection failure during transaction");
        sqlStateMessages.put("08S01", "Communication link failure");
        sqlStateMessages.put("21S01", "Insert value list does not match column list");
        sqlStateMessages.put("22003", "Numeric value out of range");
        sqlStateMessages.put("22005", "Numeric value out of range");
        sqlStateMessages.put("22008", "Datetime field overflow");
        sqlStateMessages.put("22012", "Division by zero");
        sqlStateMessages.put("28000", "Invalid authorization specification");
        sqlStateMessages.put("42000", "Syntax error or access violation");
        sqlStateMessages.put("S0001", "Base table or view already exists");
        sqlStateMessages.put("S0002", "Base table not found");
        sqlStateMessages.put("S0011", "Index already exists");
        sqlStateMessages.put("S0012", "Index not found");
        sqlStateMessages.put("S0021", "Column already exists");
        sqlStateMessages.put("S0022", "Column not found");
        sqlStateMessages.put("S0023", "No default for column");
        sqlStateMessages.put("S1000", "General error");
        sqlStateMessages.put("S1001", "Memory allocation failure");
        sqlStateMessages.put("S1002", "Invalid column number");
        sqlStateMessages.put("S1009", "Invalid argument value");
        sqlStateMessages.put("S1C00", "Driver not capable");
        sqlStateMessages.put("S1T00", "Timeout expired");
	}
	
	private String SQLState;
	private int errorCode;
	
	public DAOException( String message ) {
        super( message );
        SQLState = "";
        errorCode = -1;
    }

    public DAOException( String message, Throwable cause ) {
        super( message, cause );
        SQLState = "";
        errorCode = -1;
    }

    public DAOException( Throwable cause ) {
        super( cause );
        SQLState = "";
        errorCode = -1;
    }
    
    public DAOException(SQLException cause){
    	super(cause);
        SQLState = cause.getSQLState();
        errorCode = cause.getErrorCode();
    }
    
    @Override
    public String getLocalizedMessage(){
    	if(SQLState != ""){
    		return sqlStateMessages.get(this.SQLState);
    	}
    	else{
    		return this.getMessage();
    	}
    }
    
	public String getSQLState() {
		return SQLState;
	}
	public void setSQLState(String sQLState) {
		SQLState = sQLState;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

}

/*
Error: 1022 SQLSTATE: 23000 (ER_DUP_KEY)
Message: Can't write; duplicate key in table '%s' 

Error: 1032 SQLSTATE: HY000 (ER_KEY_NOT_FOUND)
Message: Can't find record in '%s' 
*/
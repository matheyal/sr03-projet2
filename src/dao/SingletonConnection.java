package dao;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;

// une class qui declare un objet static de type connexion afin d'avoir une connexion unique 
public class SingletonConnection {
	private static SingletonConnection instance = new SingletonConnection();
	// au moment de chargement de la classe en mémoire bloc static s'excute une seul fois
	private SingletonConnection(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static SingletonConnection getInstance(){
		return instance;
	}

	public Connection getConnection() throws SQLException{
		Connection connection = DriverManager.getConnection("jdbc:mysql://tuxa.sme.utc:3306/sr03p031","sr03p031","m4sEGUmL");
		return connection;
	}

}

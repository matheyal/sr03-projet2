package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;

import dao.SingletonConnection;
import beans.Utilisateur;

public class UtilisateurDAO {
	
	private static int nbUsers = -1;
	
	public static void printSQLException(SQLException ex) {
 	    for (Throwable e : ex) {
 	        if (e instanceof SQLException) {
                 e.printStackTrace(System.err);
                 System.err.println("SQLState: " +
                     ((SQLException)e).getSQLState());
 
                 System.err.println("Error Code: " +
                     ((SQLException)e).getErrorCode());
 
                 System.err.println("Message: " + e.getMessage());
 
                 Throwable t = ex.getCause();
                 while(t != null) {
                     System.out.println("Cause: " + t);
                     t = t.getCause();
                 }
 	        }
 	    }
 	}
	
	public static int getNbUsers() throws DAOException {
		if(nbUsers < 0){
			try{
				Connection conn= SingletonConnection.getInstance().getConnection();
				PreparedStatement ps= conn.prepareStatement("select COUNT(*) AS count from utilisateur");
				ResultSet rs = ps.executeQuery();
				rs.next();
				nbUsers = rs.getInt("count");
				rs.close();
				ps.close();
				conn.close();
			}
			catch(SQLException e){
				printSQLException(e);
				throw new DAOException(e);
			}
			
		}
		return nbUsers;
	}

	public static void addUtilisateur(Utilisateur u) throws DAOException {
		
		try {
			Connection conn= SingletonConnection.getInstance().getConnection();
			PreparedStatement ps= conn.prepareStatement("insert into utilisateur (login, email, password, admin, nom, societe, telephone, isActive)  values (?,?,?,?,?,?,?,?)");
			ps.setString(1,u.getLogin());
			ps.setString(2,u.getEmail());
			ps.setString(3,u.getMdp());
			ps.setBoolean(4,u.isAdmin());
			ps.setString(5,u.getNom());
			ps.setString(6,u.getSociete());
			ps.setString(7,u.getTelephone());
			ps.setBoolean(8,u.getStatut());
			// comme c une requette de type insert on excute la methde update
			ps.executeUpdate();
			// on ferme le preparstatment
			ps.close();
			conn.close();
		} catch (SQLException e) {
			printSQLException(e);
			throw new DAOException(e);
		}
		

	}
	
	public static Utilisateur authUtilisateur(String input_login, String input_mdp) throws DAOException {
		
		String login="", email="", mdp="", nom="", societe="", telephone="", dateCreation="";
		boolean admin=false, statut=true;
		try{
			Connection conn = SingletonConnection.getInstance().getConnection();

			PreparedStatement ps = conn.prepareStatement("SELECT * FROM utilisateur U WHERE U.login = ? AND U.password = ?");
			ps.setString(1,input_login);
			ps.setString(2,input_mdp);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				login = rs.getString("login");
				email = rs.getString("email");
				mdp = rs.getString("password");
				nom = rs.getString("nom");
				admin = rs.getBoolean("admin");
				societe = rs.getString("societe");
				telephone = rs.getString("telephone");
				dateCreation = rs.getString("dateCreation");
				statut = rs.getBoolean("isActive");
			}
			else {
				throw new DAOException("Login ou mot de passe incorrect");
			}
			
			rs.close();
			ps.close();
			conn.close();
		}
		catch(SQLException e){
			printSQLException(e);
			throw new DAOException(e);
		}
		
		return new Utilisateur(login, email, mdp, nom, admin, societe, telephone, dateCreation, statut);
	}
	
	public static Utilisateur getUtilisateur(String input_login) throws DAOException {
		
		String login="", email="", mdp="", nom="", societe="", telephone="", dateCreation="";
		boolean admin=false, statut=true;
		try{
			Connection conn = SingletonConnection.getInstance().getConnection();

			PreparedStatement ps = conn.prepareStatement("SELECT * FROM utilisateur U WHERE U.login = ?");
			ps.setString(1,input_login);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				login = rs.getString("login");
				email = rs.getString("email");
				mdp = rs.getString("password");
				nom = rs.getString("nom");
				admin = rs.getBoolean("admin");
				societe = rs.getString("societe");
				telephone = rs.getString("telephone");
				dateCreation = rs.getString("dateCreation");
				statut = rs.getBoolean("isActive");
			}
			else {
				throw new DAOException("Utilisateur " + input_login + " inexistant");
			}
			
			rs.close();
			ps.close();
			conn.close();
		}
		catch(SQLException e){
			printSQLException(e);
			throw new DAOException(e);
		}
		return new Utilisateur(login, email, mdp, nom, admin, societe, telephone, dateCreation, statut);
	}
	
	public static void modifUtilisateur(Utilisateur u) throws DAOException {
		try {
			Connection conn= SingletonConnection.getInstance().getConnection();
			PreparedStatement ps= conn.prepareStatement("UPDATE utilisateur SET email=?,password=?,admin=?,nom=?,societe=?,telephone=?,isActive=? WHERE login=?");
			ps.setString(1,u.getEmail());
			ps.setString(2,u.getMdp());
			ps.setBoolean(3,u.isAdmin());
			ps.setString(4,u.getNom());
			ps.setString(5,u.getSociete());
			ps.setString(6,u.getTelephone());
			ps.setBoolean(7,u.getStatut());
			ps.setString(8,u.getLogin());
			// comme c une requette de type insert on excute la methde update
			ps.executeUpdate();
			// on ferme le preparstatment
			ps.close();
			conn.close();
		} catch (SQLException e) {
			printSQLException(e);
			throw new DAOException(e);
		}
	}

	public static List<Utilisateur> listUtilisateurs(int debut, int nbReturn) throws DAOException {
		debut = debut<0 ? 0 : debut;
		nbReturn = nbReturn<0 ? 0 : nbReturn;
		
		List<Utilisateur> ut = new ArrayList<Utilisateur>();
		try {
			Connection conn= SingletonConnection.getInstance().getConnection();
			
			PreparedStatement ps= conn.prepareStatement("select * from utilisateur ORDER BY nom limit ?,?");
			ps.setInt(1, debut);
			ps.setInt(2, nbReturn);
			
			ResultSet rs= ps.executeQuery();
			while( rs.next()){
				String login = rs.getString("login");
				String email = rs.getString("email");
				String mdp = rs.getString("password");
				String nom = rs.getString("nom");
				boolean admin = rs.getBoolean("admin");
				String societe = rs.getString("societe");
				String telephone = rs.getString("telephone");
				String dateCreation = rs.getString("dateCreation");
				boolean statut = rs.getBoolean("isActive");
				Utilisateur u = new Utilisateur(login, email, mdp, nom, admin, societe, telephone, dateCreation, statut);

				ut.add(u);
			}
			rs.close();
			ps.close();
			conn.close();

		} catch (SQLException e) {
			printSQLException(e);
			throw new DAOException(e);
		}	

		return ut;
	}

	public static void deleteUtilisateur(String login) throws DAOException {
		try {
			Connection conn= SingletonConnection.getInstance().getConnection();
			PreparedStatement ps= conn.prepareStatement("DELETE FROM utilisateur WHERE login=?");
			ps.setString(1,login);
			// comme c une requette de type delete on excute la methde update
			ps.executeUpdate();
			// on ferme le preparstatment
			ps.close();
			conn.close();
		} catch (SQLException e) {
			printSQLException(e);
			throw new DAOException(e);
		}
	}

}

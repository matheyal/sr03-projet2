package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.Question;


public class QuestionDAO {

	public static void addQuestion(Question q) throws DAOException{

		try {
			Connection conn= SingletonConnection.getInstance().getConnection();
			PreparedStatement ps= conn.prepareStatement("insert into question (intitule, fk_test, isActive)  values (?,?,?)");
			ps.setString(1,q.getIntitule());
			ps.setInt(2,q.getIdQuestionnaire());
			ps.setBoolean(3,q.isStatut());

			ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	public static ArrayList<Question> getQuestions(int idQuestionnaire) throws DAOException{
		ArrayList<Question> lq = new ArrayList<Question>();
		try{
			Connection conn= SingletonConnection.getInstance().getConnection();
			PreparedStatement ps= conn.prepareStatement("SELECT * FROM question WHERE fk_test=?");
			ps.setInt(1, idQuestionnaire);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				String intitule = rs.getString("intitule");
				int idQuestion = rs.getInt("id_question");
				int idQuest = rs.getInt("fk_test");
				boolean isActive = rs.getBoolean("isActive");
				
				Question q = new Question(idQuestion, intitule, idQuest, isActive);
				lq.add(q);
			}
			rs.close();
			ps.close();
			conn.close();
		}
		catch(SQLException e){
			throw new DAOException(e);
		}
		return lq;
	}
}
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Questionnaire;

public class QuestionnaireDAO {
	public static void addQuestionnaire(Questionnaire qa) {

		try {
			Connection conn= SingletonConnection.getInstance().getConnection();
			PreparedStatement ps= conn.prepareStatement("insert into test (sujet,isActive)  values (?,?)");
			ps.setString(1,qa.getSujet());
			ps.setBoolean(2,qa.getStatut());

			ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}



	public static List<Questionnaire> listQuestionnaire() throws DAOException {

		List<Questionnaire> qt = new ArrayList<Questionnaire>();
		try {
			Connection conn= SingletonConnection.getInstance().getConnection();

			PreparedStatement ps= conn.prepareStatement("select * from test ORDER BY id_test ");

			ResultSet rs= ps.executeQuery();
			while( rs.next()){
				int id_test =rs.getInt("id_test");
				String sujet = rs.getString("sujet");
				boolean statut = rs.getBoolean("isActive");

				Questionnaire q = new Questionnaire(id_test,sujet, statut);

				qt.add(q);
			}
			rs.close();
			ps.close();
			conn.close();

		} catch (SQLException e) {
			throw new DAOException(e);
		}	

		return qt;
	}





	public static void modifQuestionnaire(Questionnaire q) throws DAOException {
		try {
			Connection conn= SingletonConnection.getInstance().getConnection();
			PreparedStatement ps= conn.prepareStatement("UPDATE test SET sujet=?,isActive=? WHERE id_test=?");
			ps.setString(1,q.getSujet());		
			ps.setBoolean(2,q.getStatut());
			ps.setInt(3,q.getId());
			// comme c une requette de type insert on excute la methde update
			ps.executeUpdate();
			// on ferme le preparstatment
			ps.close();
			conn.close();

		} catch (SQLException e) {
			throw new DAOException(e);
		}


	}

	public static void deleteQuestionnaire(int id) throws DAOException {
		try{
			Connection conn = SingletonConnection.getInstance().getConnection();

			PreparedStatement ps = conn.prepareStatement("DELETE FROM test WHERE id_test = ?");
			ps.setInt(1,id);

			ps.executeUpdate();

			ps.close();
			conn.close();
		}
		catch(SQLException e){
			throw new DAOException(e);
		}

	}

	public static Questionnaire getQuestionnaire( String input_sujet) throws DAOException {

		String sujet="";
		int id_test=0;
		boolean  statut=true;
		try{
			Connection conn = SingletonConnection.getInstance().getConnection();

			PreparedStatement ps = conn.prepareStatement("SELECT * FROM test WHERE sujet = ? ");
			ps.setString(1,input_sujet);

			ResultSet rs = ps.executeQuery();

			if(rs.next()){
				sujet = rs.getString("sujet");
				id_test=rs.getInt("id_test");
				statut = rs.getBoolean("isActive");
			}
			else {
				throw new DAOException("questionnaire " + input_sujet + " inexistant");
			}

			rs.close();
			ps.close();
			conn.close();
		}
		catch(SQLException e){
			throw new DAOException(e);
		}
		return new Questionnaire(id_test,sujet,statut);
	}



	public static Questionnaire getQuestionnaire(Integer id) throws DAOException{
		String sujet="";
		int id_test=0;
		boolean statut=true;
		try{
			Connection conn = SingletonConnection.getInstance().getConnection();

			PreparedStatement ps = conn.prepareStatement("SELECT * FROM test WHERE id_test = ?");
			ps.setInt(1,id);

			ResultSet rs = ps.executeQuery();

			if(rs.next()){
				sujet = rs.getString("sujet");
				id_test=rs.getInt("id_test");
				statut = rs.getBoolean("isActive");
			}
			else {
				throw new DAOException("questionnaire " + id + " inexistant");
			}

			rs.close();
			ps.close();
			conn.close();
		}
		catch(SQLException e){
			throw new DAOException(e);
		}
		return new Questionnaire(id_test,sujet,statut);
	}
}

package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Utilisateur;
import dao.DAOException;
import dao.UtilisateurDAO;

@WebServlet("/authentification")
public class AuthentificationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		String login = (String) request.getParameter("input-login");
		String pwd = (String) request.getParameter("input-pwd");
		
		try{
			Utilisateur u = UtilisateurDAO.authUtilisateur(login, pwd);
			request.getSession().setAttribute("login", u.getLogin());
			request.getSession().setAttribute("isAdmin", u.isAdmin());
			
			if (u.isAdmin()){
				this.getServletContext().getRequestDispatcher("/WEB-INF/sectionAdmin.jsp").forward( request, response );
				return;
			}
			else if (!u.isAdmin()){
				this.getServletContext().getRequestDispatcher("/WEB-INF/sectionStagiaire.jsp").forward( request, response );
				return;
			}
		}
		catch(DAOException e){
			String errorMessage = e.getLocalizedMessage();
			request.setAttribute("err", errorMessage);
			this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward( request, response );
			return;
		}
		
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		doPost(request, response);
	}
}

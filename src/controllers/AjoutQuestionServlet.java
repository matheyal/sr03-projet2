package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Question;
import beans.Questionnaire;
import dao.DAOException;
import dao.QuestionDAO;
import dao.QuestionnaireDAO;

@WebServlet("/ajoutQuestion")
public class AjoutQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		String intitule = (String) request.getParameter("input-intitule");
		Integer idQuestionnaire = null;
		if(request.getParameter("id-questionnaire") != null){
			idQuestionnaire = Integer.parseInt(request.getParameter("id-questionnaire"));
		}
		
		String sujetQuestionnaire = (String) request.getParameter("input-sujet");
		
		
		if(request.getParameter("ajoutReponse") != null){
			if(intitule != null && idQuestionnaire != null ){
				try{
					Question q = new Question(intitule, idQuestionnaire);
					QuestionDAO.addQuestion(q);
					this.getServletContext().getRequestDispatcher("/WEB-INF/Questionnaires/ajoutRepQues.jsp").forward( request, response );
					return;
				}
				catch(DAOException e){
					String errorMessage = e.getMessage();
					request.setAttribute("err", errorMessage);
					request.getRequestDispatcher("/WEB-INF/Questionnaires/ajoutQuestion.jsp").forward( request, response );
					return;
				}
				
			}
			else if(idQuestionnaire != null){
				try{
					Questionnaire q = QuestionnaireDAO.getQuestionnaire(idQuestionnaire);
					request.setAttribute("questionnaire", q);
					request.getRequestDispatcher("/WEB-INF/Questionnaires/ajoutQuestion.jsp").forward(request, response);
					return;
				}
				catch(DAOException e){
					String errorMessage = e.getMessage();
					request.setAttribute("err", errorMessage);
					request.getRequestDispatcher("gestionQuestionnaires").forward( request, response );
					return;
				}
			}
			else {
				String errorMessage = "Informations manquantes";
				request.setAttribute("err", errorMessage);
				request.getRequestDispatcher("/WEB-INF/Questionnaires/ajoutQuestion.jsp").forward( request, response );
				return;

			}
		}
		else if(request.getParameter("terminer") != null){
			String successMessage = "Le questionnaire " + sujetQuestionnaire + " a bien été mis jour";
			request.setAttribute("success", successMessage);
			request.getRequestDispatcher("gestionQuestionnaires").forward( request, response );
		}
		else if (request.getAttribute("new-questionnaire") != null){
			Questionnaire newQ = (Questionnaire) request.getAttribute("new-questionnaire");
			try{
				Questionnaire q = QuestionnaireDAO.getQuestionnaire(newQ.getSujet());
				request.setAttribute("questionnaire", q);
				request.getRequestDispatcher("/WEB-INF/Questionnaires/ajoutQuestion.jsp").forward(request, response);
				return;
			}
			catch(DAOException e){
				String errorMessage = e.getMessage();
				request.setAttribute("err", errorMessage);
				request.getRequestDispatcher("gestionQuestionnaires").forward( request, response );
				return;
			}
		}
		else{
			request.getRequestDispatcher("/WEB-INF/Questionnaires/ajoutQuestion.jsp").forward( request, response );
			return;
		}

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		doPost(request,response);
	}
}


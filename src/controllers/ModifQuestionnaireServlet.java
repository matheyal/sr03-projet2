package controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Question;
import beans.Questionnaire;
import dao.DAOException;
import dao.QuestionDAO;
import dao.QuestionnaireDAO;

@WebServlet("/modifQuestionnaire")
public class ModifQuestionnaireServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if(request.getParameter("modifier") != null){
			int id = Integer.parseInt(request.getParameter("input-id"));
			String sujet = (String) request.getParameter("input-sujet");
			boolean statut = request.getParameter("input-statut") != null;


			Questionnaire q = new Questionnaire(id, sujet, statut);
			try{
				QuestionnaireDAO.modifQuestionnaire(q);
				String successMessage = "Le questionnaire " + q.getSujet() + " a bien ete modifié !";
				request.setAttribute("succes", successMessage);
				request.getRequestDispatcher("gestionQuestionnaires").forward( request, response );
				return;
			}
			catch(DAOException e){
				String errorMessage = e.getMessage();
				request.setAttribute("err", errorMessage);
				request.getRequestDispatcher("gestionQuestionnaires").forward( request, response );
				return;
			}
		}
		else if(request.getParameter("supprimer") != null){
			int id = Integer.parseInt(request.getParameter("input-id"));
			String sujet = (String) request.getParameter("input-sujet");

			try{
				QuestionnaireDAO.deleteQuestionnaire(id);
				String successMessage = "Le questionnaire " + sujet + " a bien ete supprimé !";
				request.setAttribute("succes", successMessage);
				request.getRequestDispatcher("gestionQuestionnaires").forward( request, response );
				return;
			}
			catch(DAOException e){
				String errorMessage = e.getMessage();
				request.setAttribute("err", errorMessage);
				request.getRequestDispatcher("gestionQuestionnaires").forward( request, response );
				return;
			}
		}
		else if ()
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("idQuestionnaire") != null){
			int idQuestionnaire = Integer.parseInt(request.getParameter("idQuestionnaire"));
			try{
				Questionnaire q = QuestionnaireDAO.getQuestionnaire(idQuestionnaire);
				request.setAttribute("questionnaire", q);
				
				ArrayList<Question> questionsList = QuestionDAO.getQuestions(idQuestionnaire);
				request.setAttribute("questionsList", questionsList);
				request.getRequestDispatcher("/WEB-INF/Questionnaires/modifQuestionnaire.jsp").forward(request, response);
				return;
			}
			catch(DAOException e){
				String errorMessage = e.getMessage();
				request.setAttribute("err", errorMessage);
				request.getRequestDispatcher("/WEB-INF/Questionnaires/gestionQuestionnaires.jsp").forward( request, response );
				return;
			}
		}
		else{
			/* Nous redirigeons vers la page de gestion des questionnaires si ya rien de selectioon�*/
			response.sendRedirect("gestionQuestionnaires");
			return;
		}
	}

}



package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Utilisateur;
import dao.DAOException;
import dao.UtilisateurDAO;

@WebServlet("/ajoutUtilisateur")
public class AjoutUtilisateurServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	public boolean checkAdminAndRedirect(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		if(request.getSession().getAttribute("login") != null && request.getSession().getAttribute("isAdmin") != null){
			boolean isAdmin = (boolean) request.getSession().getAttribute("isAdmin");
			if(!isAdmin){
				request.setAttribute("err", "Vous devez être administrateur pour accéder à cette ressource");
				request.getRequestDispatcher("index").forward( request, response );
				return false;
			}
		}
		else{
			//Redirection vers la page d'authentification 
			request.setAttribute("err", "Connectez-vous pour accéder à cette ressource");
			request.getRequestDispatcher("index").forward( request, response );
			return false;
		}
		return true;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		if(checkAdminAndRedirect(request, response)){
		
			String login = (String) request.getParameter("input-login");
			String nom = (String) request.getParameter("input-nom");
			String email = (String) request.getParameter("input-email");
			String mdp = (String) request.getParameter("input-mdp");
			String societe = (String) request.getParameter("input-societe");
			String telephone = (String) request.getParameter("input-telephone");
			boolean admin = request.getParameter("input-admin") != null;
			boolean statut = true;
			
			if(login == null || nom == null || email == null || mdp == null){
				String errorMessage = "Les informations saisies sont incomplètes.";
				request.setAttribute("err", errorMessage);
				request.getRequestDispatcher("/WEB-INF/Utilisateurs/ajoutUtilisateur.jsp").forward( request, response );
				return;
			}
			else if(mdp.length() < 6){
				String errorMessage = "Le mot de passe choisi est trop court (6 caractères minimum).";
				/* Nous repassons les valeurs précédemment saisies afin de re-remplir les champs après l'affichage du message d'erreur */
				request.setAttribute("inputLogin", login);
				request.setAttribute("inputNom", nom);
				request.setAttribute("inputEmail", email);
				request.setAttribute("inputMdp", mdp);
				request.setAttribute("inputSociete", societe);
				request.setAttribute("inputTelephone", telephone);
				request.setAttribute("inputAdmin", admin);
				
				request.setAttribute("err", errorMessage);
				request.getRequestDispatcher("/WEB-INF/Utilisateurs/ajoutUtilisateur.jsp").forward( request, response );
				return;
			}
			else{
				Utilisateur u = new Utilisateur(login, email, mdp, nom, admin, societe, telephone, statut);
				try{
					UtilisateurDAO.addUtilisateur(u);
					String successMessage = "L'utilisateur " + u.getLogin() + " a bien été ajouté !";
					request.setAttribute("success", successMessage);
					request.getRequestDispatcher("gestionUtilisateurs").forward( request, response );
					return;
				}
				catch(DAOException e){
					String errorMessage = e.getLocalizedMessage();
					request.setAttribute("err", errorMessage);
					request.getRequestDispatcher("/WEB-INF/Utilisateurs/ajoutUtilisateur.jsp").forward( request, response );
					return;
				}
			}
		}
	}
	public void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		if(checkAdminAndRedirect(request, response)){
			request.getRequestDispatcher("/WEB-INF/Utilisateurs/ajoutUtilisateur.jsp").forward( request, response );
		}
		return;
	}
}

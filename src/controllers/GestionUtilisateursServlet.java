package controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Utilisateur;
import dao.DAOException;
import dao.UtilisateurDAO;


@WebServlet("/gestionUtilisateurs")
public class GestionUtilisateursServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static int utilisateursParPage = 5;

	public GestionUtilisateursServlet() {
		super();
	}
	
	public boolean checkAdminAndRedirect(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		if(request.getSession().getAttribute("login") != null && request.getSession().getAttribute("isAdmin") != null){
			boolean isAdmin = (boolean) request.getSession().getAttribute("isAdmin");
			if(!isAdmin){
				request.setAttribute("err", "Vous devez être administrateur pour accéder à cette ressource");
				request.getRequestDispatcher("index").forward( request, response );
				return false;
			}
		}
		else{
			//Redirection vers la page d'authentification 
			request.setAttribute("err", "Connectez-vous pour accéder à cette ressource");
			request.getRequestDispatcher("index").forward( request, response );
			return false;
		}
		return true;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(checkAdminAndRedirect(request, response)){
			int numPage = 1; 
			if(request.getParameter("page") != null)
				numPage = Integer.parseInt(request.getParameter("page"));
			int debutAffichage = (numPage-1)*utilisateursParPage;
			
			try{
				ArrayList<Utilisateur> users = (ArrayList<Utilisateur>) UtilisateurDAO.listUtilisateurs(debutAffichage, utilisateursParPage);
				int nbUsersTotal = UtilisateurDAO.getNbUsers();
				int nbPages = (int) Math.ceil(nbUsersTotal * 1.0 / utilisateursParPage);
				
				request.setAttribute("usersList", users);
				request.setAttribute("nbUsers", users.size());
				request.setAttribute("currentPage", numPage);
				request.setAttribute("nbPages", nbPages);
				
				request.getRequestDispatcher("/WEB-INF/Utilisateurs/gestionUtilisateurs.jsp").forward( request, response );
			}
			catch(DAOException e){
				String errorMessage = e.getLocalizedMessage();
				request.setAttribute("err", errorMessage);
				request.getRequestDispatcher("/WEB-INF/Utilisateurs/gestionUtilisateurs.jsp").forward( request, response );
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(checkAdminAndRedirect(request, response)){
			doGet(request,response);
		}
	}

}

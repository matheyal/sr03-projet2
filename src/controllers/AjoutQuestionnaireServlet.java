package controllers;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Questionnaire;

import dao.QuestionnaireDAO;

@WebServlet("/ajoutQuestionnaire")
public class AjoutQuestionnaireServlet extends HttpServlet{



	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		String sujet = (String) request.getParameter("input-sujet");

		if(sujet != null ){
			Questionnaire qa = new Questionnaire(sujet);
			QuestionnaireDAO.addQuestionnaire(qa);
			request.setAttribute("new-questionnaire", qa);
			request.getRequestDispatcher("ajoutQuestion").forward( request, response );
			return;

		}
		else {
			String errorMessage = "Informations manquantes";
			request.setAttribute("err", errorMessage);
			request.getRequestDispatcher("/WEB-INF/Questionnaires/ajoutQuestionnaire.jsp").forward( request, response );
			return;

		}

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/Questionnaires/ajoutQuestionnaire.jsp").forward( request, response );
	}
}



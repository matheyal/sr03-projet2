package controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Questionnaire;
import dao.DAOException;
import dao.QuestionnaireDAO;



@WebServlet("/gestionQuestionnaires")
public class GestionQuestionnairesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{

			ArrayList<Questionnaire> questions = (ArrayList<Questionnaire>) QuestionnaireDAO.listQuestionnaire();
			request.setAttribute("questList", questions);
			request.getRequestDispatcher("/WEB-INF/Questionnaires/gestionQuestionnaires.jsp").forward( request, response );

		}
		catch(DAOException e){
			String errorMessage = e.getMessage();
			request.setAttribute("err", errorMessage);
			request.getRequestDispatcher("/WEB-INF/Questionnaires/gestionQuestionnaires.jsp").forward( request, response );
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}


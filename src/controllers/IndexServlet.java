package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/index")
public class IndexServlet extends HttpServlet {

	private static final long serialVersionUID = 6706557497205364236L;

	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
		if(request.getSession().getAttribute("login") != null && request.getSession().getAttribute("isAdmin") != null){
			//Nous avons déjà des informations de connexion
			boolean isAdmin = (boolean) request.getSession().getAttribute("isAdmin");
			if(isAdmin){
				request.getRequestDispatcher("/WEB-INF/sectionAdmin.jsp").forward( request, response );
				return;
			}
			else{
				request.getRequestDispatcher("sectionStagiaire").forward( request, response );
				return;
			}
		}
		else{
			//Redirection vers la page d'authentification 
			request.getRequestDispatcher("/WEB-INF/index.jsp").forward( request, response );
			return;
		}

    }
}

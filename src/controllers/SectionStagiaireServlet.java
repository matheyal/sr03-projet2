package controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Questionnaire;
import dao.QuestionDAO;

/**
 * Servlet implementation class SectionUtilisateurServlet
 */
@WebServlet("/sectionStagiaire")
public class SectionStagiaireServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public boolean checkUserAndRedirect(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		if(request.getSession().getAttribute("login") != null && request.getSession().getAttribute("isAdmin") != null){
			boolean isAdmin = (boolean) request.getSession().getAttribute("isAdmin");
			if(isAdmin){
				request.setAttribute("err", "Vous devez être stagiaire pour accéder à cette ressource");
				request.getRequestDispatcher("index").forward( request, response );
				return false;
			}
		}
		else{
			//Redirection vers la page d'authentification 
			request.setAttribute("err", "Connectez-vous pour accéder à cette ressource");
			request.getRequestDispatcher("index").forward( request, response );
			return false;
		}
		return true;
	}

	
    public SectionStagiaireServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	if(checkUserAndRedirect(request, response)){
    		String login = (String) request.getSession().getAttribute("login");
    		
    		/* Obtention des tests non réalisés */
    		ArrayList<Questionnaire> listeTests = new ArrayList<Questionnaire>(); //QuestionDAO.getQuestionnairesLogin(login);
    		Questionnaire q1 = new Questionnaire("Java EE");
    		Questionnaire q2 = new Questionnaire("Géographie");
    		listeTests.add(q1);
    		listeTests.add(q2);
    		request.setAttribute("listeTests", listeTests);
    		
    		/* Obtention des tests réalisés TODO*/ 
//    		ArrayList<Parcours> listeParcours = new ArrayList<Parcours>(); //QuestionDAO.getParcoursLogin(login);
//    		Parcours p1 = new Parcours("Histoire");
//    		Parcours p2 = new Parcours("Géographie");
//    		listeParcours.add(p1);
//    		listeParcours.add(p2);
//    		request.setAttribute("listeParcours", listeParcours);
    		
    		request.getRequestDispatcher("/WEB-INF/sectionStagiaire.jsp").forward( request, response );
    	}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

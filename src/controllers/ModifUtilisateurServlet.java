package controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Utilisateur;
import dao.DAOException;
import dao.UtilisateurDAO;

/**
 * Servlet implementation class ModifUtilisateurServlet
 */
@WebServlet("/modifUtilisateur")
public class ModifUtilisateurServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public boolean checkAdminAndRedirect(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		if(request.getSession().getAttribute("login") != null && request.getSession().getAttribute("isAdmin") != null){
			boolean isAdmin = (boolean) request.getSession().getAttribute("isAdmin");
			if(!isAdmin){
				request.setAttribute("err", "Vous devez être administrateur pour accéder à cette ressource");
				request.getRequestDispatcher("index").forward( request, response );
				return false;
			}
		}
		else{
			//Redirection vers la page d'authentification 
			request.setAttribute("err", "Connectez-vous pour accéder à cette ressource");
			request.getRequestDispatcher("index").forward( request, response );
			return false;
		}
		return true;
	}
       
    public ModifUtilisateurServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(checkAdminAndRedirect(request, response)){
			if (request.getParameter("login") != null){
				String login = (String) request.getParameter("login");
				try{
					Utilisateur u = UtilisateurDAO.getUtilisateur(login);
					request.setAttribute("user", u);
					request.getRequestDispatcher("/WEB-INF/Utilisateurs/modifUtilisateur.jsp").forward(request, response);
					return;
				}
				catch(DAOException e){
					String errorMessage = e.getLocalizedMessage();
					request.setAttribute("err", errorMessage);
					request.getRequestDispatcher("/WEB-INF/Utilisateurs/ajoutUtilisateur.jsp").forward( request, response );
					return;
				}
			}
			else{
				/* Nous redirigeons vers la page d'ajout utilisateur si aucun utilisateur n'était précisé */
				response.sendRedirect("ajoutUtilisateur");
			}
		}
		
	}
	
	/* méthode appelée pour valider des modifications*/
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(checkAdminAndRedirect(request, response)){
			// Le boutton modifier a été cliqué
			if(request.getParameter("modifier") != null){
				String login = (String) request.getParameter("input-login");
				String nom = (String) request.getParameter("input-nom");
				String email = (String) request.getParameter("input-email");
				String mdp = (String) request.getParameter("input-mdp");
				String societe = (String) request.getParameter("input-societe");
				String telephone = (String) request.getParameter("input-telephone");
				boolean admin = request.getParameter("input-admin") != null;
				boolean statut = true;
				
				if(login == null || nom == null || email == null || mdp == null){
					String errorMessage = "Les informations saisies sont incomplètes.";
					request.setAttribute("err", errorMessage);
					request.getRequestDispatcher("/WEB-INF/Utilisateurs/modifUtilisateur.jsp").forward( request, response );
					return;
				}
				else{
					Utilisateur u = new Utilisateur(login, email, mdp, nom, admin, societe, telephone, statut);
					try{
						UtilisateurDAO.modifUtilisateur(u);
						String successMessage = "L'utilisateur " + u.getLogin() + " a bien été modifié !";
						request.setAttribute("succes", successMessage);
						request.getRequestDispatcher("gestionUtilisateurs").forward( request, response );
						return;
					}
					catch(DAOException e){
						String errorMessage = e.getLocalizedMessage();
						request.setAttribute("err", errorMessage);
						request.getRequestDispatcher("/WEB-INF/Utilisateurs/ajoutUtilisateur.jsp").forward( request, response );
						return;
					}
				}
			}
			// Le boutton supprimer a été clické
			else if (request.getParameter("supprimer") != null) { 
				String loginSuppr = (String) request.getParameter("input-login");
				if(loginSuppr != null){
					try{
						UtilisateurDAO.deleteUtilisateur(loginSuppr);
						String successMessage = "L'utilisateur " + loginSuppr + " a bien été supprimé !";
						request.setAttribute("succes", successMessage);
						/* Redirection vers la page de gestion des utilisateurs une fois la modification faite*/
						request.getRequestDispatcher("gestionUtilisateurs").forward( request, response );
						return;
					}
					catch(DAOException e){
						String errorMessage = e.getLocalizedMessage();
						request.setAttribute("err", errorMessage);
						request.getRequestDispatcher("/WEB-INF/Utilisateurs/ajoutUtilisateur.jsp").forward( request, response );
						return;
					}
				}
				else {
					response.sendRedirect("gestionUtilisateurs");
					return;
				}
			}
		}
		
	}

}

package beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Questionnaire implements Serializable {
	private static final long serialVersionUID = -1L;
	
	private String sujet;
	private List<Question> questions;
	private boolean statut;
	private int id;
	
	public Questionnaire(String sujet){
		super();
		this.sujet = sujet;
		this.questions = new ArrayList<Question>();
		this.statut = true;
	}
	
	public Questionnaire(int id, String sujet) {
		super();
		this.id = id;
		this.sujet = sujet;
		this.questions = new ArrayList<Question>();
		this.statut = true;
	}
	
	public Questionnaire(int id_test, String sujet, boolean statut) {
		super();
		this.id = id_test;
		this.sujet = sujet;
		this.questions = new ArrayList<Question>();
		this.statut = statut;
	}
	
	
	public int getNbQuestions(){
		return questions.size();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSujet() {
		return sujet;
	}
	public void setSujet(String sujet) {
		this.sujet = sujet;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	public boolean getStatut() {
		return statut;
	}
	public void setStatut(boolean statut) {
		this.statut = statut;
	}
	

}

package beans;

import java.io.Serializable;

public class Question implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int idQuestion;
	private String intitule;
	private int idQuestionnaire;
	private boolean statut;

	public Question(int idQuestion, String intitule, int idQuestionnaire, boolean statut) {
		super();
		this.idQuestion = idQuestion;
		this.intitule = intitule;
		this.idQuestionnaire = idQuestionnaire;
		this.statut = statut;
	}

	public Question(String intitule, int idQuestionnaire) {
		this.intitule = intitule;
		this.idQuestionnaire = idQuestionnaire;
		this.statut = true;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public boolean isStatut() {
		return statut;
	}

	public void setStatut(boolean statut) {
		this.statut = statut;
	}

	public int getIdQuestionnaire() {
		return idQuestionnaire;
	}

	public void setIdQuestionnaire(int idQuestionnaire) {
		this.idQuestionnaire = idQuestionnaire;
	}

	public int getIdQuestion() {
		return idQuestion;
	}

	public void setIdQuestion(int idQuestion) {
		this.idQuestion = idQuestion;
	}
	
	
	
}
	
package beans;

import java.io.Serializable;

public class Utilisateur implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String login = "";
	private String email = "";
	private String mdp = "";
	private String nom = "";
	private boolean admin = false;
	private String societe = "";
	private String telephone = "";
	private String dateCreation = "";
	private boolean statut = true;
	
	public Utilisateur(String login, String email, String mdp, String nom, boolean admin, String societe,
			String telephone, String dateCreation, boolean statut_compte) {
		super();
		this.login = login;
		this.email = email;
		this.mdp = mdp;
		this.nom = nom;
		this.admin = admin;
		this.societe = societe;
		this.telephone = telephone;
		this.dateCreation = dateCreation;
		this.statut = statut_compte;
	}
	
	/* Contructeur sans date de création utilisé lors de la création d'un utilisateur depuis le site */
	public Utilisateur(String login, String email, String mdp, String nom, boolean admin, String societe,
			String telephone, boolean statut_compte) {
		super();
		this.login = login;
		this.email = email;
		this.mdp = mdp;
		this.nom = nom;
		this.admin = admin;
		this.societe = societe;
		this.telephone = telephone;
		this.statut = statut_compte;
	}
	
	public Utilisateur(String login, String email, String mdp){
		super();
		this.login = login;
		this.email = email;
		this.mdp = mdp;
	}
	
	public Utilisateur(){
		super();
	}
	
	
	/*
	 * Getters and setters	
	 */
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public String getSociete() {
		return societe;
	}
	public void setSociete(String societe) {
		this.societe = societe;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}
	public boolean getStatut() {
		return statut;
	}
	public void setStatut(boolean statut_compte) {
		this.statut = statut_compte;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
}
